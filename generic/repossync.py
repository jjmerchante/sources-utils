# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2020 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#   Quan Zhou <quan@bitergia.com>
#   Valerio Cosentino <valcos@bitergia.com>
#


import argparse
import copy
import datetime
import json
import logging
import sys
import time

import requests
import gitlab
import simplejson


GITLAB_URL = 'https://gitlab.com'

GITHUB_API_URL = "https://api.github.com"

GIT = 'git'
GITHUB = 'github'
GITHUB_REPO = 'github:repo'
GERRIT = 'gerrit'
GIT_EXTENSION = '.git'

MASTER_BRANCH = 'master'
UPDATE_ACTION = 'update'

COMMIT_MSG = "{} sync by RepOSSync"
PROJECTS_JSON = "projects.json"

FILTER_RAW_REPO = '{} --filter-raw=data.project:{}'
GERRIT_PROJECTS_URL = '{}/projects/'
OPENDEV_ORG_REPOS = "{}/orgs/{}/repos"
REPO_HTML_URL = 'html_url'

RATE_LIMIT_HEADER = "X-RateLimit-Remaining"
RATE_LIMIT_RESET_HEADER = "X-RateLimit-Reset"
MIN_RATE_LIMIT = 10


def datetime_utcnow():
    """Return the current date and time in UTC."""

    return datetime.datetime.now(datetime.timezone.utc)


class GitLabFileManager:
    """GitLab file manager.

    This class allows to load, upload and commit the projects.json to
    GitLab using the library python-gitlab (version == 1.9.0). Furthermore,
    it provides additional functionalities to remove and add repositories
    to the projects.json
    """
    def __init__(self, token):
        self.token = token
        self.gl = gitlab.Gitlab(GITLAB_URL, private_token=self.token)

    def load_file(self, project_id, filename):
        """Load a file from GitLab.

        :param project_id: Gitlab project id
        :param filename: file name

        :return: file ("utf-8")
        """
        project = self.gl.projects.get(project_id)

        f = project.files.get(file_path=filename, ref=MASTER_BRANCH)
        return f.decode().decode("utf-8")

    def upload_file(self, project_id, filename, content, message):
        """Upload a file to GitLab.

        :param project_id: Gitlab project id
        :param filename: file name
        :param content: content
        :param message: commit message
        """
        project = self.gl.projects.get(project_id)
        data = {
            "branch": MASTER_BRANCH,
            "commit_message": message,
            "actions": [
                {
                    'action': UPDATE_ACTION,
                    'file_path': filename,
                    'content': content
                }
            ]
        }
        project.commits.create(data)

    @staticmethod
    def update_projects(project_file, project_name, repos):
        """Update the projects.json file with the new repositories.

        :param project_file: projects.json
        :param project_name: the name of the project to update in the projects.json
        :param repos: a list of repos of the following shape
            [
                {
                    'git': ..,
                    'github/gerrit': ...
                },
                ...
            ]
        """
        updated_project_file = copy.deepcopy(project_file)
        if project_name not in project_file:
            updated_project_file[project_name] = {}

        for repo in repos:
            for data_source in repo.keys():
                repo_in_data_source = repo[data_source]

                # if the data source is `git`, check that
                # (i) the repo ending with .git and (ii) the repo not ending in .git
                # don't exist in the projects.json. If this is the case, add the repo
                # ending with .git to the projects.json. This implies that the repos not
                # ending in .git will be kept, while new repos will be always added with
                # the .git extension. This logic is needed to avoid updating the projects.json
                # and re-collect the git raw data for the repos not ending with .git (note that
                # the re-collection would be required since for the same repo, the uuids differ
                # when it ends with/without .git)
                if data_source == GIT:
                    if ((repo_in_data_source
                         not in updated_project_file[project_name][data_source]) and
                            (repo_in_data_source + GIT_EXTENSION
                             not in updated_project_file[project_name][data_source])):
                        updated_project_file[project_name][data_source].append(repo_in_data_source + GIT_EXTENSION)
                else:
                    if repo_in_data_source not in updated_project_file[project_name][data_source]:
                        updated_project_file[project_name][data_source].append(repo[data_source])

                        if data_source == GITHUB and GITHUB_REPO in updated_project_file[project_name]:
                            updated_project_file[project_name][GITHUB_REPO].append(repo[data_source])

        # Update the repos if the name of the data source contains the word "github"
        # with the data source "github".
        # i.e. github:prs, github:pull, github2, githubql, etc.
        # They will have the same repos as "github"
        data_sources = updated_project_file[project_name].keys()
        github = "github"
        for ds in data_sources:
            if github not in ds or ds == github:
                continue
            updated_project_file[project_name][ds] = updated_project_file[project_name][github]

        return updated_project_file


def sleep_for_rate(response):
    """Sleep until the token is ready to be reused

    :param response: HTTP response
    """
    rate_limit_reset_ts = response.headers.get(RATE_LIMIT_RESET_HEADER, None)
    rate_limit = response.headers.get(RATE_LIMIT_HEADER, None)
    if rate_limit_reset_ts and rate_limit:
        rate_limit = int(rate_limit)
        rate_limit_reset_ts = int(rate_limit_reset_ts)
        current_time_ts = datetime_utcnow().replace(microsecond=0).timestamp() + 1
        time_to_reset = rate_limit_reset_ts - current_time_ts
        time_to_reset = 0 if time_to_reset < 0 else time_to_reset
        if time_to_reset and rate_limit < MIN_RATE_LIMIT:
            time.sleep(time_to_reset)


def fetch(url, headers=None, auth=None):
    """Fetch the data from an URL

    :param url: target URL
    :param headers: request's headers
    :param auth: credentials
    """
    try:
        r = requests.get(url, headers=headers, auth=auth)
        r.raise_for_status()
        sleep_for_rate(r)
    except requests.exceptions.HTTPError as error:
        if error.response.status_code == 403:
            sleep_for_rate(r)
            r = requests.get(url, headers=headers)
        else:
            raise error

    return r


def derive_filter_raw_repo(url, filter_raw_url):
    """Derive the filtered raw repo from an URL and the filter-raw URL

    :param url: repo URL
    :param filter_raw_url: filter-raw URL
    """
    repo = '/'.join(url.split('/')[-2:])
    filter_raw_repo = FILTER_RAW_REPO.format(filter_raw_url, repo)

    return filter_raw_repo


def fetch_gerrit_repos(org, api_url, opendev, filter_raw_url=None,
                       user=None, password=None, rename=None):
    """Fetch gerrit repos.

    :param org: organization
    :param api_url: gerrit API URL
    :param opendev: gerrit OpenDev instance True | False
    :param filter_raw_url: Filter raw URL
    :param user: gerrit username
    :param password: gerrit password
    :param rename: gerrit rename URL

    :return: a list of repos
    """
    def add_repos(data):
        repos = []

        for repo in data:
            new_repo = {
                GIT: repo[REPO_HTML_URL]
            }
            if filter_raw_url:
                new_repo[GERRIT] = derive_filter_raw_repo(repo[REPO_HTML_URL], filter_raw_url)
            repos.append(new_repo)

        return repos

    repos = []

    auth = None
    if user and password:
        auth = (user, password)

    if opendev:
        url = OPENDEV_ORG_REPOS.format(api_url, org)
    else:
        url = GERRIT_PROJECTS_URL.format(api_url)
    r = fetch(url, auth=auth)

    try:
        data = r.json()
    except simplejson.errors.JSONDecodeError:
        data_raw = r.text.split('\n', 1)[1]
        data_json = json.loads(data_raw)
        data = [{REPO_HTML_URL: rename + repo} for repo in data_json]
    repos.extend(add_repos(data))

    return repos


def fetch_github_repos(org, token=None):
    """Fetch github repositories.

    :param org: github organization

    :return: a list of repos
    """
    def add_repos(r):
        repos = []

        for repo in r.json():
            fork = repo.get("fork", None)
            if fork:
                continue
            elif fork is None:
                msg = "Unexpected JSON format {}".format(repo)
                logging.error(msg)
                continue

            repos.append(
                {
                    GIT: repo[REPO_HTML_URL],
                    GITHUB: repo[REPO_HTML_URL]
                }
            )

        return repos

    headers = {}
    if token:
        headers = {'Authorization': 'token ' + token}

    repos = []

    url = "{}/users/{}/repos".format(GITHUB_API_URL, org)
    page = 1
    last_page = 0

    url_page = url + "?page=" + str(page)
    r = fetch(url_page, headers=headers)

    repos.extend(add_repos(r))

    if 'last' in r.links:
        last_url = r.links['last']['url']
        last_page = last_url.split('page=')[1]
        last_page = int(last_page)

    while page < last_page:
        page += 1
        url_page = url + "?page=" + str(page)
        r = fetch(url_page, headers=headers)

        repos.extend(add_repos(r))

    return repos


def check_args(data_source, filter_raw_url, gerrit_opendev=False):
    """Check the args `data_source` and `filter_raw_url`

    :param data_source: data source to update
    :param filter_raw_url: filter raw URL
    :param gerrit_opendev: gerrit OpenDev instance True | False
    """
    if not filter_raw_url and data_source == GERRIT and gerrit_opendev:
        msg = "Gerrit requires the param `--filter-raw-url`"
        logging.error(msg)
        sys.exit(-1)

    if filter_raw_url and data_source == GITHUB:
        msg = "param `--filter-raw-url` not needed for GitHub"
        logging.warning(msg)


def sync_project(gitlab_token, gitlab_project_id, org_name, project_name,
                 data_source, filter_raw_url, github_token=None, gerrit_user=None, gerrit_password=None,
                 gerrit_api_url=None, gerrit_rename=None, gerrit_opendev=False):
    """Sync the GitHub or Gerrit repos together with their corresponding Git
    repos in the projects.json.

    :param gitlab_token: GitLab token
    :param gitlab_project_id: GitLab project ID
    :param org_name: Org name (in GitHub or OpenDev or Wikimedia)
    :param project_name: the name of the project to update in the projects.json
    :param data_source: the data source to update (Github or Gerrit)
    :param filter_raw_url: filter raw URL
    :param github_token: GitHub token, it can be `None`
    :param gerrit_user: gerrit username
    :param gerrit_password: gerrit password
    :param gerrit_api_url: gerrit API URL
    :param gerrit_rename: gerrit rename URL
    :param gerrit_opendev: gerrit OpenDev instance True | False

    :return: commit message
    """
    # check the arguments
    check_args(data_source, filter_raw_url, gerrit_opendev)

    # init gitlab file manager
    gl = GitLabFileManager(gitlab_token)

    # load projects.json
    projects_json_raw = gl.load_file(gitlab_project_id, PROJECTS_JSON)
    projects_json = json.loads(projects_json_raw)

    # fetch the upstream repos
    if data_source == GITHUB:
        repos = fetch_github_repos(org_name, token=github_token)
    else:
        repos = fetch_gerrit_repos(org_name, gerrit_api_url, gerrit_opendev, filter_raw_url=filter_raw_url,
                                   user=gerrit_user, password=gerrit_password, rename=gerrit_rename)

    # update the projects.json
    updated_projects_json = gl.update_projects(projects_json, project_name, repos)
    updated_projects_json_raw = json.dumps(updated_projects_json, indent=4, sort_keys=True)

    # commit the changes
    commit_msg = COMMIT_MSG.format(org_name)
    gl.upload_file(gitlab_project_id, PROJECTS_JSON, updated_projects_json_raw, commit_msg)
    return commit_msg


def parse_args():
    """Parse command line arguments."""

    parser = argparse.ArgumentParser()
    parser.add_argument("--gitlab-token",
                        help="GitLab token")
    parser.add_argument("--github-token", default=None,
                        help="GitHub token")
    parser.add_argument("--gitlab-project-id",
                        help="GitLab project id")
    parser.add_argument("--org-name",
                        help="Organization to sync")
    parser.add_argument("--project-name",
                        help="Project name in the projects.json")
    parser.add_argument("--source", choices=[GERRIT, GITHUB],
                        help="Data source to sync")
    parser.add_argument("--filter-raw-url", default=None,
                        help="Filter-raw URL")
    parser.add_argument("--gerrit-user", default=None,
                        help="Gerrit user")
    parser.add_argument("--gerrit-password", default=None,
                        help="Gerrit password")
    parser.add_argument("--gerrit-api-url", default=None,
                        help="Gerrit API URL")
    parser.add_argument("--gerrit-rename", default=None,
                        help="Gerrit rename URL")
    parser.add_argument("--gerrit-opendev", choices=[True, False],
                        default=False, help="Gerrit OpenDev instance")

    return parser.parse_args()


if __name__ == '__main__':
    """Sync the GitHub or Gerrit repos together with
    their corresponding Git repos in the projects.json.

    This script retrieves the projects.json located in GitLab, and
    depending on the `source` (i.e., GitHub or Gerrit) and the `org_name`
    selected, it fetches the repos from the GitHub or OpenDev APIs. Then,
    it updates the projects.json with the new repos for the given `org_name`,
    and finally commits the changes to the GitLab repository hosting
    the projects.json with the commit message `COMMIT_MSG`.
    Note that in case the `source` is GitHub and the section `github:repo` is 
    contained in the projects.json, the repos in that section are 
    automatically updated.

    Example of executions:
        repossync
            --gitlab-token ...
            --gitlab-project-id ...
            --org-name starlingx
            --project-name StarlingX
            --filter-raw-url review.opendev.org
            --source gerrit
            --gerrit-api-url https://opendev.org/api/v1
            --gerrit-opendev true

        repossync
            --gitlab-token ...
            --gitlab-project-id ...
            --org-name wikimedia
            --project-name Wikimedia
            --filter-raw-url gerrit.wikimedia.org
            --source gerrit
            --gerrit-api-url https://gerrit.wikimedia.org/r/a
            --gerrit-rename https://my.gerrit.org (optional)
            --gerrit-user ... (optional)
            --gerrit-password ... (optional)

        repossync
            --gitlab-token ...
            --gitlab-project-id ...
            --org-name Bitergia
            --project-name Bitergia
            --source github
            --github-token ... (optional)
    """
    args = parse_args()

    github_token = args.github_token
    gitlab_token = args.gitlab_token
    gitlab_project_id = args.gitlab_project_id
    org_name = args.org_name
    project_name = args.project_name
    data_source = args.source
    filter_raw_url = args.filter_raw_url
    user = args.gerrit_user
    password = args.gerrit_password
    gerrit_api_url = args.gerrit_api_url
    gerrit_rename = args.gerrit_rename
    gerrit_opendev = args.gerrit_opendev

    commit_msg = sync_project(gitlab_token, gitlab_project_id, org_name, project_name, data_source,
                              filter_raw_url, github_token=github_token, gerrit_user=user, gerrit_password=password,
                              gerrit_api_url=gerrit_api_url, gerrit_rename=gerrit_rename, gerrit_opendev=gerrit_opendev)
    msg = "{}\n\t{}".format(project_name, commit_msg)
    print(msg)
