# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Authors:
#   Quan Zhou <quan@bitergia.com>
#


import argparse
import copy
from datetime import datetime

import elasticsearch as es
import elasticsearch_dsl as dsl
import gitlab
import yaml

from github_team_info import exist_file_gitlab, upload_file


GITLAB_URL = 'https://gitlab.com'
MASTER_BRANCH = 'master'
CREATE = 'create'
UPDATE = 'update'

DOMAINS = ['magento.com', 'ebay.com', 'adobe.com', 'transoftgroup.com']

COMMUNITY_MAINTAINERS = 'Community Maintainers'
COMMUNITY_CONTRIBUTORS = 'Community Contributors'
EMPLOYEE = 'Employee'
MULTI_ORG_NAMES = [COMMUNITY_CONTRIBUTORS, EMPLOYEE]


def main():
    """Complete the affiliations YAML file with first and last commit date.

    Fetch commit date from enriched index but only under these conditions:
    - Enrolled to COMMUNITY_CONTRIBUTORS and EMPLOYEE
    - The domain of the commits is one of the DOMAINS

    For these affiliations will create an organization EMPLOYEE and merged
    with the rest of the organizations without overlapping.

    conf.yml
    ```
    es_host: https://localhost:9200
    index: git_enriched
    affiliations: affiliations.yml
    gitlab:
      token: <GitLab_TOKEN>
      project_id: <GitLab_Project_ID>
    ```

    The file `affiliations.yml` is created by the script `github_team_info.py` and `github_csv_to_yaml.py`

    Before
    ```
    - email:
      - quan@bitergia.com
      enrollments:
      - end: 2100-01-01
        organization: Community Contributors
        start: 1900-01-01
      - end: 2100-01-01
        organization: Individual Contributor
        start: 1900-01-01
      github:
      - zhquan
      profile:
        name: Quan zhou
    ```
    After
    ```
    - email:
      - quan@bitergia.com
      enrollments:
      - end: 2018-07-09
        organization: Community Contributors
        start: 1900-01-01
      - end: 2100-01-01
        organization: Community Contributors
        start: 2019-02-25
      - end: 2018-07-09
        organization: Individual Contributor
        start: 1900-01-01
      - end: 2100-01-01
        organization: Individual Contributor
        start: 2019-02-25
      - end: 2019-02-25
        organization: Employee
        start: 2018-07-09
      github:
      - zhquan
      profile:
        name: Quan zhou
    ```

    Execute:
    ```
    python3 git_first_last_commit_date_to_yaml.py <conf.yml>
    ```
    """
    args = parse_args()
    conf_name = args.conf

    conf = read_yaml(conf_name)
    index = conf['index']
    es_host = conf['es_host']
    yaml_name = conf['affiliations']

    yaml_file = read_yaml(yaml_name)

    author_commits = fetch_author_commit_date(es_host, index)
    yaml_with_date = add_date_yaml(yaml_file, author_commits)

    write_yaml(yaml_name, yaml_with_date)
    gitlab_opt = conf.get('gitlab', None)
    if gitlab_opt:
        gitlab_upload(yaml_with_date, yaml_name, gitlab_opt)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("conf", help="YAML config file")
    args = parser.parse_args()

    return args


def __get_header(token):
    header = {'PRIVATE-TOKEN': token}

    return header


def read_yaml(filename):
    with open(filename) as file:
        data = yaml.load(file, Loader=yaml.FullLoader)
    return data


def write_yaml(file_name, data):
    with open(file_name, 'w') as file:
        yaml.Dumper.ignore_aliases = lambda *args: True
        yaml.dump(data, file, allow_unicode=True, default_flow_style=False)


def fetch_author_commit_date(es_host, index):
    """Fetch min and max commit date per author.

    {
        '<author_name>': {
            'min': 2015-01-17,
            'max': 2019-01-17,
            'commits_date': [
                2015-01-17,
                2017-01-17,
                2019-01-17
            ]
    }

    :param es_host: ES host
    :param index: index
    :return: dict
    """
    def compose_should():
        should = []
        for domain in DOMAINS:
            query = dsl.Q('match', git_author_domain=domain)
            should.append(query)
        return should

    def compose_must():
        must = []
        for org in MULTI_ORG_NAMES:
            query = dsl.Q('match', author_multi_org_names=org)
            must.append(query)
        return must

    q = dsl.Q(
        'bool',
        must=compose_must(),
        should=compose_should(),
        minimum_should_match=1
    )
    client = es.Elasticsearch(es_host, timeout=90)
    s = dsl.Search(using=client, index=index).query(q)
    s = s.source(['author_name', 'author_date'])

    authors = {}
    for hit in s.scan():
        author_name = hit['author_name']
        author_date = datetime.strptime(hit['author_date'], '%Y-%m-%dT%H:%M:%S').date()
        if author_name not in authors:
            authors[author_name] = {
                'min': author_date,
                'max': author_date,
                'commit_date': [author_date]
            }
        else:
            authors[author_name]['commit_date'].append(author_date)
            authors[author_name]['min'] = min(authors[author_name]['commit_date'])
            authors[author_name]['max'] = max(authors[author_name]['commit_date'])
    return authors


def add_date_yaml(yaml_file, author_commits):
    """Add the team `Employee` if the name is on author_commits
    without overlapping.

    :param yaml_file: affiliations YAML file
    :param author_commits: author with first and last commit date

    :return: affiliations YAML file with Employee date
    """
    aux = copy.deepcopy(yaml_file)
    for uuid in aux:
        name = uuid['profile']['name']
        if name not in author_commits.keys():
            continue
        orgs = uuid['enrollments']
        if len(orgs) == 1 and orgs[0]['organization'] == EMPLOYEE:
            continue
        min_date = author_commits[name]['min']
        max_date = author_commits[name]['max']
        employee = {
            'organization': EMPLOYEE,
            'start': min_date,
            'end': max_date
        }
        new_enrollments = fix_overlapping(max_date, min_date, orgs)
        new_enrollments.append(employee)

        uuid['enrollments'] = new_enrollments
    return aux


def fix_overlapping(max_date, min_date, orgs):
    """Fix overlapping date between Employee and the rest
    except COMMUNITY_MAINTAINER.

    :param max_date: Employee max date
    :param min_date: Employee min date
    :param orgs: List of organizations

    :return: List of organizations without overlapping
    """
    new_enrollments = []
    for org in orgs:
        if org['organization'] == EMPLOYEE:
            continue
        elif org['organization'] == COMMUNITY_MAINTAINERS:
            new_enrollments.append(org)
            continue
        start = org['start']
        end = org['end']
        if start < min_date < end:
            before = {
                'organization': org['organization'],
                'start': start,
                'end': min_date
            }
            new_enrollments.append(before)
        if start < max_date < end:
            after = {
                'organization': org['organization'],
                'start': max_date,
                'end': end
            }
            new_enrollments.append(after)
    return new_enrollments


def gitlab_upload(affiliations_yaml_file, affiliations_yaml_name, gitlab_opt):
    """Upload files to GitLab

    :param affiliations_yaml_file: affiliation file
    :param affiliations_yaml_name: affiliation file name
    :param gitlab_opt: GitLab info: token and project ID
    """
    project_id = gitlab_opt['project_id']
    token = gitlab_opt['token']
    gl = gitlab.Gitlab(GITLAB_URL, private_token=token)

    action = CREATE
    if exist_file_gitlab(gl, project_id, affiliations_yaml_name):
        action = UPDATE
    affiliations_data = yaml.dump(affiliations_yaml_file, allow_unicode=True, default_flow_style=False)
    upload_file(gl, action, project_id, affiliations_yaml_name, affiliations_data)


if __name__ == '__main__':
    main()
