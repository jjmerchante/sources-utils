# Copyright (C) 2015-2020 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Quan Zhou <quan@bitergia.com>
#


import argparse
import copy
import csv
import datetime
import hashlib
import io

import gitlab
import requests
import yaml


EMPLOYEES_TEAM = 'magento-employees'
MEMBER_ROL_ONLY = ['Community Contributors', 'statistic-magento-community-skip-list']
PARENT_ID = '2545906'
PARENT_TEAM = 'magento'

GITHUB_API_URL = 'https://api.github.com'
CHILD_TEAMS_URL = GITHUB_API_URL + '/organizations/{}/team/{}/teams'
ORGS_URL = GITHUB_API_URL + '/orgs/{}'
TEAMS_URL = GITHUB_API_URL + '/orgs/{}/teams'
TEAMS_MEMBERS_URL = GITHUB_API_URL + '/organizations/{}/team/{}/members'
USERS_URL = GITHUB_API_URL + '/users/{}'

GITLAB_URL = 'https://gitlab.com'
MASTER_BRANCH = 'master'
CREATE = 'create'
UPDATE = 'update'

CSV_HEADER = ['github_login', 'name', 'email', 'github_org', 'github_team', 'first_date', 'last_date']

FIRST_DATE = '1970-01-01'
LAST_DATE = '2100-01-01'


def main():
    """Fetch information from GitHub users belonging to organization teams.
    From that information, a CSV and hierarchy of COMMUNITY_CONTRIBUTORS YAML files
    are produced where the organization and team membership is updated over time,
    updating the dates when the changes occur.

    The argument `employees` is the `magento-employees` members CSV file name and the
    team EMPLOYEES_TEAM (`magento-employees`) is skipped.

    The CSV files will be uploaded to the GitLab repository.

    The very first execution will set the `first_date` to `1970-01-01` and the
    `last_date` to `2100-01-01`. The next executions will update those dates
    according to the changes.

    i.e. CSV file:
    github_login,name,email,github_org,github_team,first_date,last_date
    zhquan,Quan Zhou,quan@bitergia.com,chaoss,grimoirelab,1900-01-01,2100-01-01

    i.e. hierarchy YAML file:
    - Partners A:
      - A: []
      - B: []
    - Partners B:
      - C: []
      - D: []

    Execution:
    $ python3 github_team_info.py <github-token> employees.csv affiliations.csv hierarchy.yml magento <gitlab-token> 17276563 --print
    """
    args = parse_args()
    gh_token = args.gh_token
    github_orgs = args.orgs
    affiliations_name = args.affiliations
    hierarchy_name = args.hierarchy
    employees_name = args.employees

    gl_token = args.gl_token
    project_id = args.project_id
    archive_csv = gitlab_read_csv(gl_token, project_id, affiliations_name)
    archive = csv_to_list(archive_csv)
    archive_hash = add_hash(archive)

    employees_csv = gitlab_read_csv(gl_token, project_id, employees_name)
    employees = csv_to_list(employees_csv, employees=True)

    users = load_users(archive)
    headers = {'Authorization': 'token ' + gh_token}
    data = []
    for org in github_orgs:
        org_id = get_org_id(org, headers)
        teams = fetch_teams(org, headers)
        data += fetch_members(headers, org_id, org, teams, users)

    data.extend(employees)
    data_hash = add_hash(data)
    data_updated = update_csv(data_hash, archive_hash)

    org_id = get_org_id(PARENT_TEAM, headers)
    hierarchy = fetch_hierarchy(org_id, PARENT_ID, headers)
    hierarchy_yml = yaml.dump(hierarchy, allow_unicode=True, default_flow_style=False)

    csv_file = io.StringIO()
    write_csv(csv_file, data_updated)

    gitlab_upload(gl_token, project_id, csv_file.getvalue(), affiliations_name)
    gitlab_upload(gl_token, project_id, hierarchy_yml, hierarchy_name)

    if args.print:
        with open(hierarchy_name, 'w') as file:
            yaml.dump(hierarchy, file, allow_unicode=True, default_flow_style=False)
        with open(affiliations_name, 'w', newline='') as csv_file:
            write_csv(csv_file, data_updated)


def parse_args():
    """Parse command line arguments"""

    parser = argparse.ArgumentParser()
    parser.add_argument("gh_token",
                        help="GitHub token")
    parser.add_argument("employees",
                        help="Employees CSV file")
    parser.add_argument("affiliations",
                        help="Affiliations CSV file")
    parser.add_argument("hierarchy",
                        help="output hierarchy YAML file")
    parser.add_argument("orgs", nargs="+",
                        help="GitHub orgs")
    parser.add_argument("gl_token",
                        help="GitLab token")
    parser.add_argument("project_id",
                        help="GitLab project ID")
    parser.add_argument("--print", action="store_true",
                        help="Write affiliations and hierarchy files")

    return parser.parse_args()


def gitlab_upload(gl_token, project_id, file, file_name):
    """Upload file to GitLab repo

    :param gl_token: GitLab token
    :param project_id: GitLab project ID
    :param file: string
    :param file_name: file name
    """
    gl = gitlab.Gitlab(GITLAB_URL, private_token=gl_token)
    action = CREATE
    if exist_file_gitlab(gl, project_id, file_name):
        action = UPDATE
    upload_file(gl, action, project_id, file_name, file)


def gitlab_read_csv(gl_token, project_id, file_name):
    gl = gitlab.Gitlab(GITLAB_URL, private_token=gl_token)
    project = gl.projects.get(project_id)
    file = project.files.raw(file_path=file_name, ref='master')
    employees_str = file.decode()
    employees_lines = employees_str.splitlines()
    csv_reader = csv.reader(employees_lines)

    return csv_reader


def csv_to_list(csv_reader, employees=False):
    """Read a CSV file with the following format:
    github_login,name,email,github_org,github_team,first_date,last_date

    And returns a list of items with the following format:
    [
        {'github_login': <login>},
        {'name': <name>},
        {'email': <email>},
        {'github_org': <org>},
        {'github_team': <team>},
        {'first_date': <date>},
        {'last_date': <date>}
    ]

    If the parameter `employees` is True the followings fields are not in `csv_reader` and
    they will set with default values:
    - github_org = PARENT_TEAM
    - github_team = EMPLOYEES_TEAM
    - first_date = FIRST_DATE
    - last_date = LAST_DATE

    :param: csv_reader: CSV reader
    :param: employees: True | False

    :return: list of users info
    """
    data = []
    line = 0
    for row in csv_reader:
        if line == 0:
            key = row
        else:
            user = {}
            for i, val in enumerate(key):
                user[val] = row[i]
            if employees:
                user['github_org'] = PARENT_TEAM
                user['github_team'] = EMPLOYEES_TEAM
                user['first_date'] = FIRST_DATE
                user['last_date'] = LAST_DATE
            data.append(user)
        line += 1

    return data


def add_hash(data):
    """Create hash as dict keys

    The content of data must have the following format:
    [
        {'github_login': <login>},
        {'name': <name>},
        {'email': <email>},
        {'github_org': <org>},
        {'github_team': <team>},
        {'first_date': <date>},
        {'last_date': <date>}
    ]

    :param data: list of dict

    :return: list of dict with hash keys
    """
    new_data = {}
    for d in data:
        team = d['github_team'] if d['github_team'] else ''
        string = d['github_login'] + d['github_org'] + team
        hash_key = create_hash(string)
        new_data[hash_key] = d

    return new_data


def load_users(data):
    """Get users info (github_login, name. email) of a list of users:
    [
        {'github_login': <login>},
        {'name': <name>},
        {'email': <email>},
        {'github_org': <org>},
        {'github_team': <team>},
        {'first_date': <date>},
        {'last_date': <date>}
    ]

    And return a dict with GitHub users info:
    {
        <github_login>: {
            'name': <name>,
            'email': <email>
        }
    }

    :param data: input list

    :return: dict with GitHub users info
    """
    users = {}
    for d in data:
        if d['github_login'] not in users:
            users[d['github_login']] = {
                'email': d['email'],
                'name': d['name']
            }
    return users


def get_org_id(org, headers):
    """Get GitHub organization ID

    :param org: GitLab organization name
    :param headers: GitLab headers
    :return: GitLab organization ID
    """
    url = ORGS_URL.format(org)
    r = get_data(url, headers)
    org_id = r.json()['id']

    return org_id


def fetch_hierarchy(org_id, team_id, headers):
    """Fetch the hierarchy for a GitHub team.

    :param org_id: GitHub organization ID
    :param team_id: GitHub team ID
    :param headers: GitHub headers
    :return: hierarchy
    """
    child_teams = fetch_child_teams(org_id, team_id, headers)
    if not child_teams:
        return []

    output = [{team['name']: []} for team in child_teams]
    for i, child in enumerate(child_teams):
        output[i][child['name']] = fetch_hierarchy(org_id, child['id'], headers)

    return output


def fetch_child_teams(org_id, parent_id, headers):
    """Fetch child teams

    :param org_id: GitHub organization ID
    :param parent_id: GitHub parent team ID
    :param headers: GitHub headers
    :return: list of child teams
    """
    url = CHILD_TEAMS_URL.format(org_id, parent_id)
    page = 1
    last_page = 0
    url_page = '{}?page={}'.format(url, page)
    r = get_data(url_page, headers)
    child_teams = get_teams_id(r.json())

    if 'last' in r.links:
        last_url = r.links['last']['url']
        last_page = last_url.split('page=')[1]
        last_page = int(last_page)

    while page < last_page:
        page += 1
        url_page = '{}?page={}'.format(url, page)
        r = get_data(url_page, headers)
        child_teams += get_teams_id(r.json())
    return child_teams


def fetch_teams(org, headers):
    """Get GitHub Teams under a organization
    {
        <github_team>: {<user1>, <user2>, ...}
    }

    :param org: GitHub organization
    :param headers: request headers

    :return: dict
    """
    url = TEAMS_URL.format(org)
    page = 1
    last_page = 0
    url_page = '{}?page={}'.format(url, page)
    r = get_data(url_page, headers)
    teams = get_teams_id(r.json())

    if 'last' in r.links:
        last_url = r.links['last']['url']
        last_page = last_url.split('page=')[1]
        last_page = int(last_page)

    while page < last_page:
        page += 1
        url_page = '{}?page={}'.format(url, page)
        r = get_data(url_page, headers)
        teams += get_teams_id(r.json())

    print("Organization: {}, Number of teams: {}".format(org, len(teams)))

    return teams


def get_teams_id(data):
    """Get teams id and name from data
    [
        {
            'id': <id>,
            'name': <name>
        }
    ]

    :param data: dict

    :return: list
    """
    teams = []
    for team in data:
        if team['name'] == EMPLOYEES_TEAM:
            continue
        new_team = {
            'id': team['id'],
            'name': team['name']
        }
        teams.append(new_team)
    return teams


def fetch_members(headers, org_id, org, teams, users_file):
    """Fetch members info under teams

    :param headers: request headers
    :param org_id: GitHub organization ID
    :param org: GitHub organization
    :param teams: GitHub teams
    :param users_file: dict with GitHub users info

    :return: list
    """
    users = []
    for team in teams:
        logins = fetch_logins(headers, org_id, team)
        print("Organization: {}, Team: {}, fetching users info...".format(org, team['name']))
        if logins:
            new_users = fetch_info_users(headers, logins, org, team['name'], users_file)
            print("Organization: {}, Team: {}, users: {}".format(org, team['name'], len(new_users)))
            users += new_users
        else:
            print("Organization: {}, Team: {}, users: 0".format(org, team['name']))

    return users


def fetch_logins(headers, org_id, team):
    """Fetch GitHub logins under a GitHub team

    :param headers: request headers
    :param org_id: GitHub organization ID
    :param team: GitHub organization team

    :return: list
    """
    url = TEAMS_MEMBERS_URL.format(org_id, team['id'])
    query_conexion = '?'
    if team['name'] in MEMBER_ROL_ONLY:
        url = url + '?role=member'
        query_conexion = '&'
    page = 1
    last_page = 0
    url_page = '{}{}page={}'.format(url, query_conexion, page)
    r = get_data(url_page, headers)
    logins = fetch_items(r.json(), 'login')
    if 'last' in r.links:
        last_url = r.links['last']['url']
        last_page = last_url.split('page=')[1]
        last_page = int(last_page)
    while page < last_page:
        page += 1
        url_page = '{}{}page={}'.format(url, query_conexion, page)
        r = get_data(url_page, headers)
        logins += fetch_items(r.json(), 'login')

    return logins


def update_csv(data, archive):
    """Update archive CSV with the new data

    :param data: new data
    :param archive: archived data in CSV

    :return: archive updated
    """
    aux_data = copy.deepcopy(data)
    if not archive:
        return aux_data

    aux_archive = copy.deepcopy(archive)

    set_data = set(aux_data)
    set_archive = set(aux_archive)
    diff_data = set_data.difference(set_archive)
    diff_archive = set_archive.difference(set_data)

    now = datetime.datetime.now()
    date_time = now.strftime("%Y-%m-%d")

    # New employee member
    for d in aux_data:
        if d in diff_data:
            aux_data[d]['first_date'] = date_time

    # Leave employee member or update info
    for a in aux_archive:
        # Leave employee
        if a in diff_archive and aux_archive[a]['last_date'] == LAST_DATE:
            aux_archive[a]['last_date'] = date_time
        if a not in aux_data:
            continue

        # Update name and/or email according to the employees.yml
        if aux_data[a]['name'] != aux_archive[a]['name']:
            aux_archive[a]['name'] = aux_data[a]['name']
        if aux_data[a]['email'] != aux_archive[a]['email']:
            aux_archive[a]['email'] = aux_data[a]['email']

        # Set last_date to LAST_DATE (2100-01-01)
        aux_archive[a]['last_date'] = LAST_DATE

    aux_data.update(aux_archive)

    return aux_data


def write_csv(csv_file, data):
    writer = csv.writer(csv_file, delimiter=',',
                        quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(CSV_HEADER)
    for d in sorted(data.keys()):
        row = [
            data[d]['github_login'],
            data[d]['name'],
            data[d]['email'],
            data[d]['github_org'],
            data[d]['github_team'],
            data[d]['first_date'],
            data[d]['last_date']
        ]
        writer.writerow(row)


def fetch_items(data, key):
    """Fetch items depends of the key.

    :param data: JSON
    :param key: name of the key that you want to fetch
    :returns: list of the items
    """
    items = []
    for repo in data:
        items.append(repo[key])

    return items


def fetch_info_users(headers, logins, org, team, users_file):
    """Fetch users info and returns a list with the following format:
    [
        {
            'github_login': <login>,
            'github_org': org,
            'github_team': team,
            'first_date': FIRST_DATE,
            ¡last_date': LAST_DATE,
            'name': <name>,
            'email': <email>
        }
    ]

    :param headers: request headers
    :param logins: list of members
    :param org: GitHub organization
    :param team: GitHub team
    :param users_file: dict with GitHub users info

    :return: list
    """
    users = []
    for login in logins:
        if login not in users_file:
            url = USERS_URL.format(login)
            r = get_data(url, headers)
            user_data = r.json()
            new_user = {
                'name': user_data['name'],
                'email': user_data['email']
            }
            users_file[login] = new_user
        user = {
            "github_login": login,
            "github_org": org,
            "github_team": team,
            "first_date": FIRST_DATE,
            "last_date": LAST_DATE,
            "name": users_file[login]['name'],
            "email": users_file[login]['email']
        }
        users.append(user)

    return users


def get_data(url, headers=None):
    r = requests.get(url, headers=headers)
    r.raise_for_status()
    return r


def create_hash(string):
    """Create a hexadecimal hash

    :param string: input string

    :return: hash
    """
    hash_key = hashlib.sha1(string.encode('UTF-8', errors="surrogateescape")).hexdigest()
    return hash_key


def exist_file_gitlab(gl, project_id, filename):
    """Check if the file does exist in GitLab.

    :param gl: gitlab
    :param project_id: Gitlab project id
    :param filename: file name

    :return: True | False
    """
    project = gl.projects.get(project_id)
    try:
        project.files.get(file_path=filename, ref=MASTER_BRANCH)
        exist = True
    except gitlab.exceptions.GitlabGetError:
        exist = False

    return exist


def upload_file(gl, action, project_id, filename, content):
    """Upload a file to GitLab.

    :param gl: gitlab
    :param action: create | update
    :param project_id: Gitlab project id
    :param filename: file name
    :param content: content
    """
    message = "{} updated automatically".format(filename)
    data = {
        "branch": MASTER_BRANCH,
        "commit_message": message,
        "actions": [
            {
                'action': action,
                'file_path': filename,
                'content': content
            }
        ]
    }

    project = gl.projects.get(project_id)
    project.commits.create(data)


if __name__ == '__main__':
    main()
