#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Authors:
#     Luis Cañas-Díaz <lcanas@bitergia.com>
#

import argparse
import json
import logging
import sys

import group_by_datasources


DESC_MSG = "Print via stdout a summary of the repos found in the projects JSON file"

def main():
    args = parse_arguments()
    json_file = args.json_file

    with open(json_file, 'r') as myfile:
        data = json.load(myfile)

    bn = group_by_datasources.extract_backend_names(data)
    summary = {}
    for item in bn:
        repos = group_by_datasources.extract_repos_by_backend(data, item)
        n_repos = len(repos)

        root_backend = item.split(':')[0]

        if root_backend in summary.keys():
            summary[root_backend] = summary[root_backend] + n_repos
        else:
            summary[root_backend] = n_repos

    for item in summary:
        print("{}: {}".format(item,summary[item]))

def parse_arguments():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=DESC_MSG)

    parser.add_argument('json_file', action='store', help='input JSON file')
    args = parser.parse_args()
    return args

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        s = "\n\nReceived Ctrl-C or other break signal. Exiting.\n"
        sys.stdout.write(s)
        sys.exit(0)
    except RuntimeError as e:
        s = "Error: %s\n" % str(e)
        sys.stderr.write(s)
        sys.exit(1)
    except FileNotFoundError as e:
        s = "Error: %s\n" % str(e)
        sys.stderr.write(s)
        sys.exit(1)
