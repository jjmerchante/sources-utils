# Copyright (C) 2015-2020 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Quan Zhou <quan@bitergia.com>
#


import argparse
import csv
from urllib.parse import urljoin

import pymysql


HATSTALL = 'identities/hatstall/'
QUERY_UUIDS = 'SELECT identities.uuid, profiles.name, count(identities.uuid) FROM identities ' \
              'INNER JOIN profiles ON identities.uuid=profiles.uuid ' \
              'GROUP BY identities.uuid HAVING count(identities.uuid) > {};'
QUERY_EMAILS = 'SELECT email FROM identities WHERE uuid="{}";'
CSV_HEADER = ['hatstall_uuid', 'name', '#identities', 'email']


def main():
    """Fetch hatstall_uuid, name, the ID number with the
    same UUID, email associated from SortingHat database.

    The parameter `limit` is the ID number with the same UUID.
    i.e if `limit` is 10 it will get all the UUID with more
    than 9 associated IDs.

    The script generate a CSV file with the following format:
    ```
    hatstall_uuid,name,#identities,email
    http://localhost:5601/identities/hatstall/030a8ba106db40fd3964af218029a29fdd6e8be8,Quan Zhou,25,quan@bitergia.com
    ```

    Execution:
    ```
    python3 get_n_identities.py localhost test_sh 20
    ```
    """
    args = parse_args()
    host = args.host
    user = args.user
    password = args.password
    database = args.database
    limit = args.limit
    kibiter = args.kibiter

    hatstall_url = urljoin(kibiter, HATSTALL)
    db = pymysql.connect(host, user, password, database)
    uuids = fetch_uuids(db, limit, hatstall_url)
    emails = fetch_emails(db, uuids)

    log = generate_logs(uuids, emails)
    print(log)

    if args.csv:
        write_csv(args.csv, uuids, emails)


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("host",
                        help="MariaDB/MySQL host")
    parser.add_argument("database",
                        help="MariaDB/MySQL database")
    parser.add_argument("limit",
                        help="the ID number with the same UUID")
    parser.add_argument("kibiter",
                        help="Kibiter host")
    parser.add_argument("-u", "--user",
                        default="root",
                        help="MariaDB/MySQL user  (by default = 'root')")
    parser.add_argument("-p", "--password",
                        default="",
                        help="MariaDB/MySQL password (by default = '')")
    parser.add_argument("--csv",
                        help="write the output to a CSV file")
    args = parser.parse_args()

    return args


def fetch_uuids(db, limit, hatstall_url=None):
    """Fetch uuid, name, and the ID number with the same UUID
    return a list:
    [
        {
            'uuid': <uuid>,
            'name': <name>,
            'n': <id_number>
        }
    ]

    :param db: connection of the database
    :param limit: the ID number with the same UUID
    :param hatstall_url: hatstall URL

    :return: list
    """
    cursor = db.cursor()
    cursor.execute(QUERY_UUIDS.format(limit))
    data = cursor.fetchall()
    uuids = []
    for d in data:
        new = {
            'hatstall_uuid': urljoin(hatstall_url, d[0]),
            'name': d[1],
            'n': d[2]
        }
        uuids.append(new)

    return uuids


def fetch_emails(db, uuids):
    """Fetch emails info given the uuid

    It return a dict:
    ```
    {
        <hatstall_uuid>: [<email1>, <email2>, ...]
    }
    ```

    :param db: connection of the database
    :param uuids: list of uuids

    :return: emails
    """
    emails = {}
    cursor = db.cursor()
    for uuid in uuids:
        hatstall_uuid = uuid['hatstall_uuid']
        _id = hatstall_uuid.split("/")[-1]
        cursor.execute(QUERY_EMAILS.format(_id))
        data = cursor.fetchall()
        id_emails = list(filter(None, set([d[0] for d in data])))
        emails[hatstall_uuid] = id_emails

    return emails


def generate_logs(uuids, emails):
    """Generate logs based on the list of uuids and the dict of emails.

    It returns logs like:
    ```
    <hatstall_URL>/f82f2fbef236f4a5909e726046ddfc8494994f42	Quan Zhou	16	['quan@bitergia.com', 'quanzh@bitergia.com']	2
    <hatstall_URL>/5678bf8765d4567897654231bbbfff1234253213	Tom	12	['tom@tom.com']	1
    ```

    :param uuids: uuids (from fetch_uuids)
    :param emails: emails (from fetch_emails)

    :return: logs
    """
    msg = ""
    for u in uuids:
        msg += "{}\t{}\t{}\t{}\t{}\n".format(u['hatstall_uuid'], u['name'],
                                             u['n'], emails[u['hatstall_uuid']],
                                             len(emails[u['hatstall_uuid']]))

    return msg


def write_csv(filename, uuids, emails):
    """Write in a CSV file the uuids and emails info with the
    following format:
    ```
    hatstall_uuid,name,#identities,email
    <hatstall_URL>/f82f2fbef236f4a5909e726046ddfc8494994f42,Quan Zhou,16,quan@bitergia.com
    <hatstall_URL>/f82f2fbef236f4a5909e726046ddfc8494994f42,Quan Zhou,16,quanzh@bitergia.com
    <hatstall_URL>/5678bf8765d4567897654231bbbfff1234253213,Tom	12,tom@tom.com
    ```

    :param filename: CSV file name
    :param uuids: uuids (from fetch_uudis)
    :param emails: emails (from fetch_emails)
    """
    with open(filename, 'w', newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(CSV_HEADER)
        for u in uuids:
            for e in emails[u['hatstall_uuid']]:
                row = [
                    u['hatstall_uuid'],
                    u['name'],
                    u['n'],
                    e
                ]
                writer.writerow(row)


if __name__ == '__main__':
    main()
