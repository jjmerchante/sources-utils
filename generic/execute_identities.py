# Copyright (C) 2015-2020 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Quan Zhou <quan@bitergia.com>
#


import argparse
import json
import os
import sys

import pymysql

from get_n_identities import fetch_uuids, fetch_emails, generate_logs

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "/usr/local/lib/jenkins_ticket_utils/"))
from ticket_utils.phabricatorUtils import PhabricatorUtils


QUERY_DATABASES = 'show databases;'


def main():
    """Fetch the number of the identities affiliated to a unique identity and
    the emails associated in a MySQL/MaraiDB database. And create a comment
    in phabricator if the limit is exceeded.

    You can set a default limit for all of them and customize some limit
    in the JSON config file:
    {
        "phab_host": "<PHAB_host>",
        "phab_token": "<PHAB_token>",
        "phab_task_id": "<PHAB_task_ID>",
        "host": "<MySQL_host>",
        "user": "<MySQL_user>",
        "password": "<MySQL_password>",
        "limit_default": 30,
        "limits": {
            "bitergia_sh": 20,
            "chaoss_sh": 10
        }
    }
    """
    args = parse_args()
    conf_file = args.conf

    conf = read_json(conf_file)
    host = conf['host']
    user = conf['user']
    password = conf['password']
    limit_default = conf['limit_default']
    limits = conf['limits']

    databases = fetch_databases(host, user, password)
    logs = {}
    for database in databases:
        if "_sh" not in database:
            continue

        limit = limit_default
        if database in limits:
            limit = limits[database]

        print("\nDatabase: {}, limit: {}".format(database, limit))

        db = pymysql.connect(host, user, password, database)
        uuids = fetch_uuids(db, limit)
        emails = fetch_emails(db, uuids)

        if not uuids:
            print("..no profiles with more than {} identities found in {}".format(limit, database))
            continue

        log = generate_logs(uuids, emails)
        print(log)

        new = {
            "limit": limit,
            "uuids": uuids,
            "emails": emails
        }
        logs[database] = new

    phab_host = conf['phab_host']
    phab_token = conf['phab_token']
    phab_task_id = conf['phab_task_id']
    phabricator_comment(phab_host, phab_token, phab_task_id, logs)


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("conf",
                        help="configuration file")
    args = parser.parse_args()

    return args


def read_json(filename):
    with open(filename) as json_file:
        data = json.load(json_file)
    return data


def fetch_databases(host, user, password):
    """Fetch MySQL/MariaDB databases

    :param host: host
    :param user: user
    :param password: password

    :return: list of databases
    """
    db = pymysql.connect(host, user, password)
    cursor = db.cursor()
    cursor.execute(QUERY_DATABASES)
    data = cursor.fetchall()

    databases = []
    for d in data:
        databases.append(d[0])

    return databases


def phabricator_comment(host, token, task_id, logs):
    """Create a comment in PHAB

    :param host: host
    :param token: token
    :param task_id: task ID
    :param logs: logs
    """
    if not logs:
        return

    phab = PhabricatorUtils(host, token)
    phab_msg = ""
    for database in logs:
        phab_msg += "Database: {}, limit: {}\n```lines=10\n".format(database, logs[database]['limit'])
        phab_msg += generate_logs(logs[database]['uuids'], logs[database]['emails'])
        phab_msg += "```\n\n"

    phab.add_comment(task_id, phab_msg)


if __name__ == '__main__':
    main()
