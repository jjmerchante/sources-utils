# -*- coding: utf-8 -*-
#
# Copyright (C) 2019 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Authors:
#   Quan Zhou <quan@bitergia.com>
#


import json

from githubsync import main


def open_file(filename):
    with open(filename, 'r') as f:
        return json.loads(f.read())


def run():
    config_filename = "githubsync.json"
    config = open_file(config_filename)

    gl_token = config["gitlab_token"]

    projects = [project for project in config if project != "gitlab_token"]
    for project in projects:
        project_id = config[project]["project_id"]
        github_list = config[project]["github"]
        for github in github_list:
            alias = github['alias']
            gh_org = github['name']
            main(gl_token, project_id, gh_org, alias)


if __name__ == '__main__':
    run()
