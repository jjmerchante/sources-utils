# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2020 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#   Quan Zhou <quan@bitergia.com>
#


import argparse
import json
import os
import sys

from repossync import sync_project

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "/usr/local/lib/jenkins_ticket_utils/"))
from ticket_utils.phabricatorUtils import PhabricatorUtils


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('conf',
                        help="JSON config file")
    parser.add_argument('projects',
                        help="JSON projects file")
    args = parser.parse_args()

    return args


def read_json(filename):
    with open(filename) as json_file:
        data = json.load(json_file)
    return data


def execute_sync_project(project, data_source, gl_project_id, gl_token, gh_token):
    """ Execute sync_project of the script repossync.py with the parameters

    :param project: project
    :param data_source: github | gerrit
    :param gl_project_id: gitlab project id
    :param gl_token: gitlab token
    :param gh_token: github token

    :return: commit messages
    """
    msg = ""
    for repo in project[data_source]:
        alias = repo['alias']
        org_name = repo['name']
        filter_raw_url = repo.get('filter_raw_url', None)
        user = repo.get('user', None)
        password = repo.get('password', None)
        api_url = repo.get('api_url', None)
        rename = repo.get('rename', None)
        is_opendev = repo.get('is_opendev', False)
        msg_repo = sync_project(gl_token, gl_project_id, org_name, alias, data_source, filter_raw_url,
                                github_token=gh_token, gerrit_user=user, gerrit_password=password,
                                gerrit_api_url=api_url, gerrit_rename=rename, gerrit_opendev=is_opendev)
        msg += "\t{}\n".format(msg_repo)
    return msg


def main():
    args = parse_args()
    conf_name = args.conf
    projects_name = args.projects

    conf = read_json(conf_name)
    phab_host = conf["phab_host"]
    phab_token = conf["phab_token"]
    phab_task_id = conf["phab_task_id"]
    gl_token = conf["gitlab_token"]
    gh_token = None
    if "github_token" in conf:
        gh_token = conf["github_token"]

    projects = read_json(projects_name)
    msg = ""
    for project in projects:
        gl_project_id = projects[project]["project_id"]
        data_sources = [data_source for data_source in projects[project] if data_source != "project_id"]
        msg += "{}\n".format(project)
        for data_source in data_sources:
            msg += execute_sync_project(projects[project], data_source, gl_project_id, gl_token, gh_token)
    print(msg)

    # create or update ticket in phabricator
    phab = PhabricatorUtils(phab_host, phab_token)
    phab_msg = "{}\nRepossync: https://jenkins.bitergia.net/job/repossync".format(msg)
    phab.add_comment(phab_task_id, phab_msg)


if __name__ == '__main__':
    """Execute the script repossync.py to synchronize the projects.json
    data sources supported:
      - github: it will sync automatically 'github:repo' if does exists
      - gerrit

    You must to have two JSON config file. The result is written as a comment
    in a phabricator ticket (`phab_task_id`).

    * JSON file with configuration parameters.
    ```
    {
        "phab_host": "https://myphabricatorhost/api/",
        "phab_token": "<PHAB-Token>",
        "phab_task_id": <PHAB-task-id>,
        "jenkins_host": "https://myjenkinshost/",
        "gitlab_token": "<GitLab-Token>",
        "github_token": "<GitHub-Token>" (optional)
    }
    ```

    * JSON file with projects information.
    ```
    {
        "<project>": {
            "project_id": "<gitlab_project_id>",
            "github": [
                {
                    "alias": "Bitergia",
                    "name": "bitergia"
                }
            ],
            "gerrit": [
                {
                    "alias": "Chaoss",
                    "name": "chaoss",
                    "filter_raw_url": "review.opendev.org",
                    "api_url": "https://opendev.org/api/v1",
                    "is_opendev": true
                },
                {
                    "alias": "Bitergia-Gerrit",
                    "name": "bitergia-gerrit",
                    "user": "<gerrit-user>",
                    "password": "<gerrit-password>",
                    "api_url": "https://gerrit.wikimedia.org/r/a"
                }
            ]
        }
    }
    ```

    Important: you have to clone the repo https://gitlab.com/Bitergia/devops/ticket-utils
    at `/usr/local/lib/jenkins_ticket_utils`
    `git clone https://gitlab.com/Bitergia/devops/ticket-utils /usr/local/lib/jenkins_ticket_utils`

    Execution:
    `python3 execute_repossync.py conf.json projects.json`
    """

    main()
